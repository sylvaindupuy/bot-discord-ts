import { Client, ClientVoiceManager } from 'discord.js';
import Enmap from 'enmap';

export abstract class BotClient {
    private static client: Client;

    private static myEnmap: Enmap;

    static getClient(): Client {
        return this.client;
    }

    static getDb(): Enmap {
        return this.myEnmap;
    }

    static getUsername(): string | undefined {
        return this.client.user?.username;
    }

    static getId(): string | undefined {
        return this.client.user?.id;
    }

    static async getOwnerId() {
        if (!this.client.application?.owner) {
            await this.client.application?.fetch();
        }

        return this.client.application?.owner?.id;
    }

    static getAvatar(): string | undefined {
        const avatar = this.client.user?.avatarURL();

        return avatar ? avatar : undefined;
    }

    static getVoiceManager(): ClientVoiceManager | null {
        return this.client.voice;
    }

    static setClient(client: Client): void {
        this.client = client;
    }

    static setDb(myEnmap: Enmap): void {
        this.myEnmap = myEnmap;
    }

    static login(): void {
        this.client.login(process.env.TOKEN).catch((err) => console.error(err));
    }
}
