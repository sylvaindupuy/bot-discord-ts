import { generateDependencyReport } from '@discordjs/voice';
import { parse } from 'discord-command-parser';
import {
    BaseMessageOptions,
    Client,
    CommandInteraction,
    GatewayIntentBits,
    GuildChannel,
    Interaction,
    Message,
    Snowflake
} from 'discord.js';
import 'dotenv/config';
import Enmap from 'enmap';
import playDl from 'play-dl';
import { MusicSubscription } from './audio/subscription';
import { BotClient } from './bot/botClient';
import { commands } from './commands/commands';
import play from './commands/slash/play';
import { slashCommands } from './commands/slashCommands';
import { errorReply, userBanned } from './utils';

const parseOptions = { allowBots: false, allowSelf: false };
const subscriptions = new Map<Snowflake, MusicSubscription>();

playDl.getFreeClientID().then((clientID) => {
    playDl.setToken({
        soundcloud: {
            client_id: clientID,
        },
    });
});

// Normal enmap with default options
BotClient.setDb(
    new Enmap({
        name: 'botdiscord',
        autoFetch: true,
        fetchAll: false,
    })
);

BotClient.setClient(
    new Client({
        intents: [
            GatewayIntentBits.Guilds,
            GatewayIntentBits.GuildMembers,
            GatewayIntentBits.GuildMessages,
            GatewayIntentBits.GuildVoiceStates,
            GatewayIntentBits.MessageContent,
        ],
    })
);

BotClient.getClient().on('messageCreate', (msg: Message) => {
    const clientId = BotClient.getId();

    if (
        !userBanned(msg.author.id) &&
        clientId &&
        msg.mentions.users.has(clientId)
    ) {
        msg.reply('What can I do for you? (!help for more information)');
        return;
    }

    if (msg.channel instanceof GuildChannel && msg.guild != null) {
        const parsed = parse(msg, '!', parseOptions);

        if (!parsed.success) return;

        const handler = commands.get(parsed.command);

        if (userBanned(msg.author.id)) {
            msg.reply(
                errorReply(
                    'You are banned from this app !'
                ) as BaseMessageOptions
            );
        } else if (handler) {
            handler(msg);
        }
    }
});

// Handles slash command interactions
BotClient.getClient().on(
    'interactionCreate',
    async (interaction: Interaction) => {
        if (!interaction.isCommand() || !interaction.guildId) return;

        const userId = (interaction as CommandInteraction).member?.user.id;
        const handler = slashCommands.get(interaction.commandName);

        if (userBanned(userId)) {
            await interaction.reply(
                errorReply('You are banned from this app !')
            );
        } else if (handler) {
            await handler(interaction, subscriptions, interaction.guild);
        } else {
            await interaction.reply(errorReply('Unknown command'));
        }
    }
);

BotClient.getClient().on(
    'interactionCreate',
    async (interaction: Interaction) => {
        if (!interaction.isStringSelectMenu() || !interaction.guild) return;

        if (interaction.customId === 'songSelect') {
            await play(interaction, subscriptions, interaction.guild);
        }
    }
);

BotClient.getClient().on('ready', () => {
    console.info(generateDependencyReport());
    console.info('Bot is ready to use');
});

BotClient.getClient().on('error', console.warn);

BotClient.login();
