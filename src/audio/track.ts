import { AudioResource, createAudioResource } from '@discordjs/voice';
import play, { SoundCloudTrack } from 'play-dl';

/**
 * This is the data required to create a Track object.
 */
export interface TrackData {
    url: string;
    soundcloud: boolean;
    title: string;
    duration: number;
    author: string;
    imgAuthor: string;
    imgUrl: string;
    live: boolean;
    onStart: () => void;
    onFinish: () => void;
    onError: (error: Error) => void;
}

// eslint-disable-next-line @typescript-eslint/no-empty-function
const noop = () => {};

export class Track implements TrackData {
    public readonly url: string;
    public readonly soundcloud: boolean;
    public readonly title: string;
    public readonly duration: number;
    public readonly author: string;
    public readonly imgAuthor: string;
    public readonly imgUrl: string;
    public readonly live: boolean;
    public readonly onStart: () => void;
    public readonly onFinish: () => void;
    public readonly onError: (error: Error) => void;

    private constructor({
        url,
        soundcloud,
        title,
        duration,
        author,
        imgAuthor,
        imgUrl,
        live,
        onStart,
        onFinish,
        onError,
    }: TrackData) {
        this.url = url;
        this.soundcloud = soundcloud;
        this.title = title;
        this.duration = duration;
        this.author = author;
        this.imgAuthor = imgAuthor;
        this.imgUrl = imgUrl;
        this.live = live;
        this.onStart = onStart;
        this.onFinish = onFinish;
        this.onError = onError;
    }

    /**
     * Creates an AudioResource from this Track.
     */
    public createAudioResource(): Promise<AudioResource<Track>> {
        return new Promise((resolve, reject) => {
            play.stream(this.url, { seek: this.specificStart() })
                .then((stream) => {
                    resolve(
                        createAudioResource(stream.stream, {
                            metadata: this,
                            inputType: stream.type,
                        })
                    );
                })
                .catch(reject);
        });
    }

    /**
     * @returns Duration of the song
     */
    public formattedDuration(): string {
        if (this.live) {
            return 'LIVE';
        }

        if (this.duration) {
            return `${Math.floor(this.duration / 60)} minutes and ${
                this.duration % 60
            } secondes`;
        }

        return '';
    }

    /**
     * Retrieve the time to start from the url
     * @returns Number of second
     */
    private specificStart(): number {
        let time = new URL(this.url).searchParams.get('t');
        let start = 0;

        if (time) {
            // If not a number, remove the last character (probably s for second)
            time = isNaN(+time) ? time.slice(0, -1) : time;

            if (!isNaN(+time)) {
                start = Number(time);
            }
        }

        return start;
    }

    /**
     * Creates a Track from a video URL and lifecycle callback methods.
     *
     * @param url The URL of the video
     * @param methods Lifecycle callbacks
     *
     * @returns The created Track
     */
    public static async from(
        url: string,
        soundcloud: boolean,
        methods: Pick<Track, 'onStart' | 'onFinish' | 'onError'>
    ): Promise<Track> {
        // The methods are wrapped so that we can ensure that they are only called once.
        const wrappedMethods = {
            onStart() {
                wrappedMethods.onStart = noop;
                methods.onStart();
            },
            onFinish() {
                wrappedMethods.onFinish = noop;
                methods.onFinish();
            },
            onError(error: Error) {
                wrappedMethods.onError = noop;
                methods.onError(error);
            },
        };

        try {
            if (soundcloud) {
                const info = (await play.soundcloud(url)) as SoundCloudTrack;

                return new Track({
                    title: info.name,
                    url,
                    soundcloud,
                    duration: info.durationInSec,
                    live: false,
                    author: info.publisher?.artist || '',
                    imgAuthor: '',
                    imgUrl: info.thumbnail,
                    ...wrappedMethods,
                });
            } else {
                const info = await play.video_basic_info(url);
                const videoDetails = info.video_details;

                return new Track({
                    title: videoDetails.title || '',
                    url,
                    soundcloud,
                    duration: videoDetails.durationInSec,
                    live: videoDetails.live,
                    author: videoDetails.channel?.name || '',
                    imgAuthor: videoDetails.channel?.iconURL() || '',
                    imgUrl: videoDetails.thumbnails.splice(-1)[0].url,
                    ...wrappedMethods,
                });
            }
        } catch (error) {
            console.info(`Error getting video info: ${url}`);
            console.info(error);
            return new Track({
                title: '',
                url,
                soundcloud,
                duration: 0,
                live: false,
                author: '',
                imgAuthor: '',
                imgUrl: '',
                ...wrappedMethods,
            });
        }
    }
}
