import {
    AudioPlayer,
    AudioPlayerError,
    AudioPlayerPlayingState,
    AudioPlayerState,
    AudioPlayerStatus,
    AudioResource,
    createAudioPlayer,
    entersState,
    VoiceConnection,
    VoiceConnectionDisconnectedState,
    VoiceConnectionDisconnectReason,
    VoiceConnectionState,
    VoiceConnectionStatus,
} from '@discordjs/voice';
import { promisify } from 'node:util';
import type { Track } from './track';

const wait = promisify(setTimeout);

export class MusicSubscription {
    public voiceConnection: VoiceConnection;
    public readonly audioPlayer: AudioPlayer;
    public queue: Track[];
    public queueLock = false;
    public readyLock = false;
    private autoLogout: NodeJS.Timeout | undefined;

    public isOnVoiceChannel() {
        return (
            this.voiceConnection.state.status === VoiceConnectionStatus.Ready
        );
    }

    private handleVoice = async () => {
        if (!this.readyLock) {
            /**
             * In the Signalling or Connecting states, we set a 20 second time limit for the connection to become ready
             * before destroying the voice connection. This stops the voice connection permanently existing in one of these
             * states.
             */
            this.readyLock = true;
            try {
                await entersState(
                    this.voiceConnection,
                    VoiceConnectionStatus.Ready,
                    20_000
                );
            } catch {
                if (
                    this.voiceConnection.state.status !==
                    VoiceConnectionStatus.Destroyed
                )
                    this.voiceConnection.destroy();
            } finally {
                this.readyLock = false;
            }
        }
    };

    public changeVoiceChannel(voiceConnection: VoiceConnection) {
        this.voiceConnection = voiceConnection;
    }

    public constructor(voiceConnection: VoiceConnection) {
        this.voiceConnection = voiceConnection;
        this.audioPlayer = createAudioPlayer();
        this.queue = [];

        this.voiceConnection.on(
            VoiceConnectionStatus.Disconnected,
            async (
                oldState: VoiceConnectionState,
                newState: VoiceConnectionDisconnectedState
            ) => {
                if (
                    newState.reason ===
                        VoiceConnectionDisconnectReason.WebSocketClose &&
                    newState.closeCode === 4014
                ) {
                    /**
                     * If the WebSocket closed with a 4014 code, this means that we should not manually attempt to reconnect,
                     * but there is a chance the connection will recover itself if the reason of the disconnect was due to
                     * switching voice channels. This is also the same code for the bot being kicked from the voice channel,
                     * so we allow 5 seconds to figure out which scenario it is. If the bot has been kicked, we should destroy
                     * the voice connection.
                     */
                    try {
                        await entersState(
                            this.voiceConnection,
                            VoiceConnectionStatus.Connecting,
                            5_000
                        );
                        // Probably moved voice channel
                    } catch {
                        this.voiceConnection.destroy();
                        // Probably removed from voice channel
                    }
                } else if (this.voiceConnection.rejoinAttempts < 5) {
                    /**
                     * The disconnect in this case is recoverable, and we also have <5 repeated attempts so we will reconnect.
                     */
                    await wait(
                        (this.voiceConnection.rejoinAttempts + 1) * 5_000
                    );
                    this.voiceConnection.rejoin();
                } else {
                    /**
                     * The disconnect in this case may be recoverable, but we have no more remaining attempts - destroy.
                     */
                    this.voiceConnection.destroy();
                }
            }
        );

        this.voiceConnection.on(VoiceConnectionStatus.Destroyed, () => {
            this.stop();
        });

        this.voiceConnection.on(
            VoiceConnectionStatus.Connecting,
            this.handleVoice
        );

        this.voiceConnection.on(
            VoiceConnectionStatus.Signalling,
            this.handleVoice
        );

        this.audioPlayer.on(
            AudioPlayerStatus.Idle,
            (oldState: AudioPlayerState) => {
                // If the Idle state is entered from a non-Idle state, it means that an audio resource has finished playing.
                // The queue is then processed to start playing the next track, if one is available.
                if (oldState.status !== AudioPlayerStatus.Idle) {
                    (
                        oldState.resource as AudioResource<Track>
                    ).metadata.onFinish();
                    void this.processQueue();
                }
            }
        );

        this.audioPlayer.on(
            AudioPlayerStatus.Playing,
            (oldState: AudioPlayerState, newState: AudioPlayerPlayingState) => {
                // If the Playing state has been entered, then a new track has started playback.
                (newState.resource as AudioResource<Track>).metadata.onStart();
            }
        );

        this.audioPlayer.on('error', (error: AudioPlayerError) =>
            (error.resource as AudioResource<Track>).metadata.onError(error)
        );

        voiceConnection.subscribe(this.audioPlayer);
    }

    /**
     * Adds a new Track to the queue.
     *
     * @param track The track to add to the queue
     */
    public enqueue(track: Track) {
        this.queue.push(track);
        void this.processQueue();
    }

    removeFromQueue(index: number) {
        if (index > 0 && this.queue.length >= index) {
            this.queue.splice(index - 1, 1);
        } else {
            throw new Error();
        }
    }

    public clearQueue() {
        this.queue = [];
    }

    /**
     * Stops audio playback and empties the queue.
     */
    public stop() {
        clearTimeout(this.autoLogout);
        this.queueLock = true;
        this.queue = [];
        this.audioPlayer.stop(true);
    }

    private disconnect() {
        if (
            this.voiceConnection.state.status !==
            VoiceConnectionStatus.Destroyed
        ) {
            this.voiceConnection.destroy();
        }
    }

    /**
     * Attempts to play a Track from the queue.
     */
    private async processQueue(): Promise<void> {
        const emptyQueue = this.queue.length === 0;

        // If the queue is locked (already being processed), is empty, or the audio player is already playing something, return
        if (
            this.queueLock ||
            this.audioPlayer.state.status !== AudioPlayerStatus.Idle ||
            emptyQueue
        ) {
            this.autoLogout = setTimeout(() => {
                if (this.audioPlayer.state.status === AudioPlayerStatus.Idle) {
                    console.info('auto logout');
                    this.disconnect();
                }
            }, 300000);
            return;
        }

        clearTimeout(this.autoLogout);

        // Lock the queue to guarantee safe access
        this.queueLock = true;

        // Take the first item from the queue. This is guaranteed to exist due to the non-empty check above.
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const nextTrack = this.queue.shift()!;
        try {
            // Attempt to convert the Track into an AudioResource (i.e. start streaming the video)
            const resource = await nextTrack.createAudioResource();
            this.audioPlayer.play(resource);
            this.queueLock = false;
        } catch (error) {
            console.error(error);
            // If an error occurred, try the next item of the queue instead
            nextTrack.onError(error as Error);
            this.queueLock = false;
            return this.processQueue();
        }
    }
}
