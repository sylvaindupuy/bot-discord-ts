import { EmbedAuthorData, EmbedFooterData } from '@discordjs/builders';
import {
    CommandInteraction,
    EmbedBuilder,
    Guild,
    GuildMember,
    InteractionReplyOptions,
    PermissionFlagsBits,
    PermissionsBitField,
    SelectMenuInteraction,
} from 'discord.js';
import { Track } from './audio/track';
import { BotClient } from './bot/botClient';

function interactionReply(
    embed: EmbedBuilder,
    ephemeral: boolean = false
): InteractionReplyOptions {
    return {
        embeds: [embed],
        ephemeral: ephemeral,
    };
}

export function errorReply(
    title: string,
    ephemeral: boolean = false
): InteractionReplyOptions {
    return interactionReply(
        new EmbedBuilder().setColor([255, 0, 0]).setTitle(title),
        ephemeral
    );
}

export function infoReply(
    title: string,
    ephemeral: boolean = false,
    description?: string
): InteractionReplyOptions {
    const embed = new EmbedBuilder().setColor(3447003).setTitle(title);

    if (description) {
        embed.setDescription(description);
    }

    return interactionReply(embed, ephemeral);
}

export function musicReply(track: Track): InteractionReplyOptions {
    const authorData: EmbedAuthorData = {
        name: track.author,
        iconURL: track.imgAuthor,
    };
    const footerData: EmbedFooterData = {
        text: BotClient.getUsername() || '',
        iconURL: BotClient.getAvatar(),
    };

    const embed = new EmbedBuilder()
        .setColor(3447003)
        .setTitle(`:loud_sound: ${track.title}`)
        .setURL(track.url)
        .setImage(track.imgUrl)
        .addFields([{ name: 'Duration', value: track.formattedDuration() }])
        .setAuthor(authorData)
        .setFooter(footerData)
        .setTimestamp();

    return interactionReply(embed);
}

export function playlistReply(
    queue: Track[],
    title: string
): InteractionReplyOptions {
    const footerData: EmbedFooterData = {
        text: BotClient.getUsername() || '',
        iconURL: BotClient.getAvatar(),
    };

    const embed = new EmbedBuilder()
        .setTitle(title)
        .setColor(0x00ae86)
        .setFooter(footerData)
        .setTimestamp();

    queue.slice(0, 24).forEach((music, index) => {
        embed.addFields([
            {
                name: `${index + 1} - ${
                    music.author
                } (${music.formattedDuration()})`,
                value: `[${music.title}](${music.url})`,
            },
        ]);
    });

    if (queue.length > 24) {
        embed.addFields([{ name: `${queue.length - 24} more`, value: 'ㅤ' }]);
    }

    return interactionReply(embed);
}

export function userBanned(userId: string | undefined) {
    const db = BotClient.getDb();

    return db.has('bannedUsers') && db.get('bannedUsers').includes(userId);
}

export function isURL(url: string): boolean {
    const p =
        // eslint-disable-next-line no-useless-escape
        /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
    return p.test(url);
}

export function isYoutubeLink(url: string): boolean {
    const p =
        /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
    return p.test(url);
}

export function isSoundcloudLink(url: string): boolean {
    const p =
        /^(?:https?:\/\/)((?:www\.)|(?:m\.))?soundcloud\.com\/[a-z0-9](?!.*?(-|_){2})[\w-]{1,23}[a-z0-9](?:\/.+)?$/;
    return p.test(url);
}

/**
 * Check if user have admin rights (or is the bot owner)
 * @param member
 * @returns
 */
export async function haveAdminRights(member: GuildMember) {
    const ownerId = await BotClient.getOwnerId();

    return (
        member.permissions.has(PermissionsBitField.Flags.Administrator) ||
        member.id === ownerId
    );
}

export function havePermissionToJoin(
    interaction: CommandInteraction | SelectMenuInteraction,
    guild: Guild
) {
    if (
        interaction.member instanceof GuildMember &&
        interaction.member.voice.channel
    ) {
        return guild.members.me
            ?.permissionsIn(interaction.member?.voice?.channel)
            .has(PermissionFlagsBits.Connect);
    }
}
