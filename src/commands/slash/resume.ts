import { CommandInteraction, Guild, Snowflake } from 'discord.js';
import { MusicSubscription } from '../../audio/subscription';
import { errorReply, infoReply } from '../../utils';

/**
 * Take over the song
 * @param interaction
 * @param subscriptions
 * @param guildId
 */
export default async function resume(
    interaction: CommandInteraction,
    subscriptions: Map<Snowflake, MusicSubscription>,
    guild: Guild
) {
    const subscription = subscriptions.get(guild.id);

    if (subscription) {
        subscription.audioPlayer.unpause();
        await interaction.reply(infoReply('Unpaused!', true));
    } else {
        await interaction.reply(errorReply('Not playing on this server!'));
    }
}
