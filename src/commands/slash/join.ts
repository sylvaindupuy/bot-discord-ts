import {
    entersState,
    joinVoiceChannel,
    VoiceConnectionStatus,
} from '@discordjs/voice';
import { CommandInteraction, Guild, GuildMember, Snowflake } from 'discord.js';
import { MusicSubscription } from '../../audio/subscription';
import { errorReply, havePermissionToJoin, infoReply } from '../../utils';

/**
 * Join the user voice channel (if bot is playing)
 * @param interaction
 * @param subscriptions
 * @param guildId
 */
export default async function join(
    interaction: CommandInteraction,
    subscriptions: Map<Snowflake, MusicSubscription>,
    guild: Guild
) {
    let channel;
    const subscription = subscriptions.get(guild.id);
    const isOnVoiceChannel =
        interaction.member instanceof GuildMember &&
        interaction.member.voice.channel;

    await interaction.deferReply();

    // If a connection to the guild doesn't already exist and the user is in a voice channel, join that channel
    // and create a subscription.
    if (subscription) {
        if (
            interaction.member instanceof GuildMember &&
            interaction.member.voice.channel
        ) {
            channel = interaction.member.voice.channel;

            subscription.changeVoiceChannel(
                joinVoiceChannel({
                    channelId: channel.id,
                    guildId: channel.guild.id,
                    adapterCreator: channel.guild.voiceAdapterCreator,
                })
            );
            subscription.voiceConnection.on('error', console.warn);
        }
    }

    // If there is no subscription, tell the user they need to join a channel.
    if (!subscription) {
        await interaction.followUp(errorReply('Bot is not currently playing!'));
        return;
    }

    if (!isOnVoiceChannel) {
        await interaction.followUp(
            errorReply('Join a voice channel and try again!')
        );
        return;
    }

    if (!havePermissionToJoin(interaction, guild)) {
        await interaction.followUp(
            errorReply("I don't have enough permissions to join!")
        );
        return;
    }

    // Make sure the connection is ready before processing the user's request
    try {
        await entersState(
            subscription.voiceConnection,
            VoiceConnectionStatus.Ready,
            20e3
        );
        await interaction.followUp(
            infoReply(
                `:headphones: Successfully joined the voice channel **${channel?.name}**`
            )
        );
    } catch (error) {
        console.warn(error);
        await interaction.followUp(
            errorReply(
                'Failed to join the voice channel within 20 seconds, please try again later!'
            )
        );
        return;
    }
}
