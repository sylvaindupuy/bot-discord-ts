import {
    entersState,
    joinVoiceChannel,
    VoiceConnectionStatus,
} from '@discordjs/voice';
import {
    ActionRowBuilder,
    CommandInteraction,
    Guild,
    GuildMember,
    SelectMenuComponentOptionData,
    Snowflake,
    StringSelectMenuBuilder,
    StringSelectMenuInteraction,
} from 'discord.js';
import playdl, { SoundCloudPlaylist, SoundCloudTrack } from 'play-dl';
import { MusicSubscription } from '../../audio/subscription';
import { Track } from '../../audio/track';
import {
    errorReply,
    havePermissionToJoin,
    isSoundcloudLink,
    isURL,
    isYoutubeLink,
    musicReply,
} from '../../utils';

/**
 * Get the song from param or select interaction
 * @param interaction
 * @returns song
 */
function getSongFromInteraction(
    interaction: CommandInteraction | StringSelectMenuInteraction
): string {
    let song = '';

    if (interaction instanceof CommandInteraction) {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        song = interaction.options.get('song')!.value! as string;
    } else if (interaction instanceof StringSelectMenuInteraction) {
        song = interaction.values[0];
    }

    return song;
}

/**
 * Search videos corresponding to the keywords and propose he 5 first to the user
 * @param interaction
 * @param keywords
 */
async function handleKeyword(
    interaction: CommandInteraction | StringSelectMenuInteraction,
    keywords: string
) {
    const row: ActionRowBuilder<StringSelectMenuBuilder> =
        new ActionRowBuilder();
    const options: SelectMenuComponentOptionData[] = [];

    try {
        const videos = await playdl.search(keywords, {
            source: {
                youtube: 'video',
            },
        });

        if (videos.length === 0) {
            await interaction.followUp(
                errorReply(':warning: No result found !')
            );
            return;
        }

        videos.forEach((video) => {
            options.push({
                label: (video.title || ' ').substring(0, 100),
                description: (video.description || ' ').substring(0, 100),
                value: video.url,
            });
        });

        row.addComponents(
            new StringSelectMenuBuilder()
                .setCustomId('songSelect')
                .setPlaceholder('Select a song')
                .addOptions(options)
        );

        await interaction.followUp({ components: [row] });
    } catch (error) {
        console.warn(error);
        await interaction.editReply(
            errorReply(':warning: Error while searching !')
        );
    }
}

export default async function play(
    interaction: CommandInteraction | StringSelectMenuInteraction,
    subscriptions: Map<Snowflake, MusicSubscription>,
    guild: Guild
) {
    let subscription = subscriptions.get(guild.id);
    const isSelectMenu = interaction instanceof StringSelectMenuInteraction;
    const song = getSongFromInteraction(interaction);
    const youtubeLink = isYoutubeLink(song);
    const soundcloudLink = isSoundcloudLink(song);
    let songs;

    isSelectMenu
        ? await interaction.deferUpdate()
        : await interaction.deferReply();

    if (!isURL(song)) {
        await handleKeyword(interaction, song);
        return;
    }

    if (!youtubeLink && !soundcloudLink) {
        await interaction.followUp(
            errorReply(`:warning: ${song} : isn't a youtube or soundcloud link`)
        );
        return;
    }

    if (!havePermissionToJoin(interaction, guild)) {
        await interaction.followUp(
            errorReply("I don't have enough permissions to join!")
        );
        return;
    }

    try {
        // Get songs urls (from video or playlist link)
        songs = await getSongs(song, youtubeLink, soundcloudLink);
    } catch (error) {
        await interaction.followUp(
            errorReply(
                'Failed to retrieve songs urls, please try another link!'
            )
        );
        console.error(`Could not retrieve any songs from link ${song}`, error);
        return;
    }

    // If a connection to the guild doesn't already exist and the user is in a voice channel, join that channel
    // and create a subscription.
    if (!subscription || !subscription.isOnVoiceChannel()) {
        if (
            interaction.member instanceof GuildMember &&
            interaction.member.voice.channel
        ) {
            const channel = interaction.member.voice.channel;
            subscription = new MusicSubscription(
                joinVoiceChannel({
                    channelId: channel.id,
                    guildId: channel.guild.id,
                    adapterCreator: channel.guild.voiceAdapterCreator,
                })
            );
            subscription.voiceConnection.on('error', console.warn);
            subscriptions.set(guild.id, subscription);
        }
    }

    if (!subscription) {
        await interaction.followUp(
            errorReply('Join a voice channel and try again!')
        );
        return;
    }

    // Make sure the connection is ready before processing the user's request
    try {
        await entersState(
            subscription.voiceConnection,
            VoiceConnectionStatus.Ready,
            20e3
        );
    } catch (error) {
        console.warn(error);
        await interaction.followUp(
            errorReply(
                'Failed to join the voice channel within 20 seconds, please try again later!'
            )
        );
        return;
    }

    // Before Track creation to reply fast
    const text = `**${songs.length} song(s)** are being added to the playlist...`;

    if (isSelectMenu) {
        await interaction.editReply({
            content: text,
            components: [],
        });
    } else {
        await interaction.followUp(text);
    }

    try {
        for (const song of songs) {
            // Attempt to create a Track from the user's video URL
            const track = await Track.from(song, soundcloudLink, {
                onStart() {
                    interaction.followUp(musicReply(track)).catch(console.warn);
                },
                onFinish() {
                    // nothing
                },
                onError(error) {
                    console.warn(error);
                    interaction
                        .followUp({
                            content: `Error: ${error.message}`,
                            ephemeral: true,
                        })
                        .catch(console.warn);
                },
            });

            subscription.enqueue(track);
        }
    } catch (error) {
        console.warn(error);
        await interaction.editReply(
            errorReply('Failed to play track, please try again later!')
        );
    }
}
async function getSongs(
    song: string,
    youtubeLink: boolean,
    soundcloudLink: boolean
) {
    const songs: string[] = [];

    if (youtubeLink) {
        const playlistId = new URL(song).searchParams.get('list');

        if (playlistId) {
            const response = await playdl.playlist_info(song, {
                incomplete: true,
            });
            const videos = await response.all_videos();

            videos.forEach((video) => {
                songs.push(video.url);
            });
        } else {
            songs.push(song);
        }
    } else if (soundcloudLink) {
        const response = await playdl.soundcloud(song);

        if (response instanceof SoundCloudPlaylist) {
            const tracks = await response.all_tracks();

            tracks.forEach((track) => {
                songs.push((track as SoundCloudTrack).url);
            });
        } else if (response instanceof SoundCloudTrack) {
            songs.push(song);
        }
    }

    return songs;
}
