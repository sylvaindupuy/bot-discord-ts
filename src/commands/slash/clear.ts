import { CommandInteraction, Guild, Snowflake } from 'discord.js';
import { MusicSubscription } from '../../audio/subscription';
import { errorReply, infoReply } from '../../utils';

/**
 * Empty the playlist
 * @param interaction
 * @param subscriptions
 * @param guildId
 */
export default async function clear(
    interaction: CommandInteraction,
    subscriptions: Map<Snowflake, MusicSubscription>,
    guild: Guild
) {
    const subscription = subscriptions.get(guild.id);

    if (subscription) {
        subscription.clearQueue();
        await interaction.reply(infoReply('Playlist cleared !', true));
    } else {
        await interaction.reply(errorReply('Not playing on this server!'));
    }
}
