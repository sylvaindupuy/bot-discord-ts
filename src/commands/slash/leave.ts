import { CommandInteraction, Guild, GuildMember, Snowflake } from 'discord.js';
import { MusicSubscription } from '../../audio/subscription';
import { errorReply, infoReply } from '../../utils';

/**
 * Leave the voice channel (and delete musicSubscription)
 * @param interaction
 * @param subscriptions
 * @param guildId
 */
export default async function leave(
    interaction: CommandInteraction,
    subscriptions: Map<Snowflake, MusicSubscription>,
    guild: Guild
) {
    const subscription = subscriptions.get(guild.id);

    if (subscription) {
        const channelId = subscription.voiceConnection.joinConfig.channelId;
        const channels = await (
            interaction.member as GuildMember
        ).guild.channels.fetch();

        const channelName = channels.find(
            (channel) => channel?.id === channelId
        )?.name;

        subscription.voiceConnection.destroy();
        subscriptions.delete(guild.id);

        await interaction.reply(
            infoReply(
                `:headphones: Disconnected from channel: **${channelName}**`,
                true
            )
        );
    } else {
        await interaction.reply(errorReply('Not playing on this server!'));
    }
}
