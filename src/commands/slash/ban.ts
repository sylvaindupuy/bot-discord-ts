/* eslint-disable @typescript-eslint/no-unused-vars */
import {
    CommandInteraction,
    Guild,
    GuildMember,
    Snowflake,
    User,
} from 'discord.js';
import { MusicSubscription } from '../../audio/subscription';
import { BotClient } from '../../bot/botClient';
import { errorReply, haveAdminRights, infoReply } from '../../utils';

/**
 * Ban a user
 * @param interaction
 * @param subscriptions
 * @param guildId
 */
export default async function ban(
    interaction: CommandInteraction,
    subscriptions: Map<Snowflake, MusicSubscription>,
    guild: Guild
) {
    const user = interaction.options.get('user')?.user as User;
    const admin = await haveAdminRights(interaction.member as GuildMember);
    const db = BotClient.getDb();

    if (admin) {
        if (!db.has('bannedUsers')) {
            db.set('bannedUsers', []);
        }

        if (db.get('bannedUsers').includes(user.id)) {
            await interaction.reply(
                errorReply(`${user.username} has already been banned`, true)
            );
            return;
        }

        db.push('bannedUsers', user.id);

        await interaction.reply(
            infoReply(`${user.username} has been succesfully banned`, true)
        );
    } else {
        await interaction.reply(
            errorReply('Make sure you have sufficient permissions (only admin)', true)
        );
    }
}
