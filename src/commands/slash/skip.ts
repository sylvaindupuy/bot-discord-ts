import { CommandInteraction, Guild, Snowflake } from 'discord.js';
import { MusicSubscription } from '../../audio/subscription';
import { errorReply, infoReply } from '../../utils';

/**
 * Skip to the next song
 * @param interaction
 * @param subscriptions
 * @param guildId
 */
export default async function skip(
    interaction: CommandInteraction,
    subscriptions: Map<Snowflake, MusicSubscription>,
    guild: Guild
) {
    const subscription = subscriptions.get(guild.id);

    if (subscription) {
        subscription.audioPlayer.stop();
        await interaction.reply(infoReply('Song skipped!'));
    } else {
        await interaction.reply(errorReply('Not playing on this server!'));
    }
}
