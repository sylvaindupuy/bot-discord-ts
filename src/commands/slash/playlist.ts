import { AudioPlayerStatus, AudioResource } from '@discordjs/voice';
import { CommandInteraction, Guild, Snowflake } from 'discord.js';
import { MusicSubscription } from '../../audio/subscription';
import { Track } from '../../audio/track';
import { infoReply, playlistReply } from '../../utils';

/**
 * Show the current playlist (the 5 first songs)
 * @param interaction
 * @param subscriptions
 * @param guildId
 */
export default async function playlist(
    interaction: CommandInteraction,
    subscriptions: Map<Snowflake, MusicSubscription>,
    guild: Guild
) {
    const subscription = subscriptions.get(guild.id);

    // Print out the current queue, including up to the next 5 tracks to be played.
    if (subscription) {
        const title =
            subscription.audioPlayer.state.status === AudioPlayerStatus.Idle
                ? 'Nothing playing!'
                : `Current: **${
                      (
                          subscription.audioPlayer.state
                              .resource as AudioResource<Track>
                      ).metadata.title
                  }**`;

        await interaction.reply(playlistReply(subscription.queue, title));
    } else {
        await interaction.reply(infoReply('Not playing on this server!'));
    }
}
