/* eslint-disable @typescript-eslint/no-unused-vars */
import { CommandInteraction, Guild, Snowflake } from 'discord.js';
import { version as pkgVersion } from '../../../package.json';
import { MusicSubscription } from '../../audio/subscription';
import { infoReply } from '../../utils';

/**
 * Get the current version of the bot (and link to the git tag)
 * @param interaction
 * @param subscriptions
 * @param guildId
 */
export default async function version(
    interaction: CommandInteraction,
    subscriptions: Map<Snowflake, MusicSubscription>,
    guild: Guild
) {
    const versionTxt = 'v' + pkgVersion;

    await interaction.reply(
        infoReply(
            versionTxt,
            true,
            `[git](https://gitlab.com/sylvaindupuy/bot-discord-ts/-/tags/${versionTxt})`
        )
    );
}
