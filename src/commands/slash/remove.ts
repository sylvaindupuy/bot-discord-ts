/* eslint-disable @typescript-eslint/no-unused-vars */
import { CommandInteraction, Guild, Snowflake } from 'discord.js';
import { MusicSubscription } from '../../audio/subscription';
import { errorReply, infoReply } from '../../utils';

/**
 * Remove a particular song from the playlist
 * @param interaction
 * @param subscriptions
 * @param guildId
 */
export default async function remove(
    interaction: CommandInteraction,
    subscriptions: Map<Snowflake, MusicSubscription>,
    guild: Guild
) {
    const subscription = subscriptions.get(guild.id);

    if (subscription) {
        const index = interaction.options.get('index')?.value as number;

        try {
            subscription.removeFromQueue(index);
            await interaction.reply(
                infoReply(`the music n°${index} has been removed`, true)
            );
        } catch (error) {
            await interaction.reply(errorReply('Invalid index'));
        }
    } else {
        await interaction.reply(errorReply('Not playing on this server!'));
    }
}
