import { CommandInteraction, Guild, Snowflake } from 'discord.js';
import { MusicSubscription } from '../../audio/subscription';
import { errorReply, infoReply } from '../../utils';

/**
 * Pause the currently playing song
 * @param interaction
 * @param subscriptions
 * @param guildId
 */
export default async function pause(
    interaction: CommandInteraction,
    subscriptions: Map<Snowflake, MusicSubscription>,
    guild: Guild
) {
    const subscription = subscriptions.get(guild.id);

    if (subscription) {
        subscription.audioPlayer.pause();
        await interaction.reply(infoReply('Paused!', true));
    } else {
        await interaction.reply(errorReply('Not playing on this server!'));
    }
}
