import { EmbedFooterData, SlashCommandBuilder } from '@discordjs/builders';
import { EmbedBuilder, Message, MessagePayload } from 'discord.js';
import { BotClient } from '../bot/botClient';
import { errorReply, haveAdminRights, infoReply } from '../utils';

export const commands = new Map();

async function deploy(msg: Message) {
    const admin = msg.member && haveAdminRights(msg.member);

    const playCmd = new SlashCommandBuilder()
        .setName('play')
        .setDescription('Play a youtube or soundcloud song')
        .addStringOption((param) =>
            param
                .setName('song')
                .setDescription(
                    'The URL or name of the song / playlist to play'
                )
                .setRequired(true)
        )
        .toJSON();

    const skipCmd = new SlashCommandBuilder()
        .setName('skip')
        .setDescription('Skip to the next song in the queue')
        .toJSON();

    const playlistCmd = new SlashCommandBuilder()
        .setName('playlist')
        .setDescription('See the music playlist')
        .toJSON();

    const pauseCmd = new SlashCommandBuilder()
        .setName('pause')
        .setDescription('Pauses the song that is currently playing')
        .toJSON();

    const resumeCmd = new SlashCommandBuilder()
        .setName('resume')
        .setDescription('Resume playback of the current song')
        .toJSON();

    const leaveCmd = new SlashCommandBuilder()
        .setName('leave')
        .setDescription('Leave the voice channel')
        .toJSON();

    const versionCmd = new SlashCommandBuilder()
        .setName('version')
        .setDescription('Version of the bot')
        .toJSON();

    const clearCmd = new SlashCommandBuilder()
        .setName('clear')
        .setDescription('Empty the playlist')
        .toJSON();

    const banCmd = new SlashCommandBuilder()
        .setName('ban')
        .setDescription('Ban a user')
        .addUserOption((param) =>
            param
                .setName('user')
                .setDescription('The user to ban')
                .setRequired(true)
        )
        .toJSON();

    const unbanCmd = new SlashCommandBuilder()
        .setName('unban')
        .setDescription('Ban a user')
        .addUserOption((param) =>
            param
                .setName('user')
                .setDescription('The user to ban')
                .setRequired(true)
        )
        .toJSON();

    const removeCmd = new SlashCommandBuilder()
        .setName('remove')
        .setDescription('Remove a sound from the playlist')
        .addNumberOption((param) =>
            param
                .setName('index')
                .setDescription('Index of the song to remove')
                .setRequired(true)
        )
        .toJSON();

    const joinCmd = new SlashCommandBuilder()
        .setName('join')
        .setDescription(
            'Join your voice channel (only if music is currently playing)'
        )
        .toJSON();

    if (msg.guild && admin) {
        try {
            await msg.guild.commands.set([
                playCmd,
                skipCmd,
                playlistCmd,
                pauseCmd,
                resumeCmd,
                leaveCmd,
                versionCmd,
                clearCmd,
                banCmd,
                unbanCmd,
                removeCmd,
                joinCmd,
            ]);

            await msg.reply('Deployed!');
        } catch (error) {
            await msg.reply(
                'Error Deploying, make sure the bot has sufficient rights (scope applications.commands)'
            );
        }
    } else {
        await msg.reply(
            errorReply(
                'Only admin can deploy commands (or the bot owner)'
            ) as unknown as MessagePayload
        );
    }
}

async function help(msg: Message) {
    const reply = infoReply('List of orders');
    const embeds = reply.embeds as EmbedBuilder[];
    const footerData: EmbedFooterData = {
        text: BotClient.getUsername() || '',
        iconURL: BotClient.getAvatar(),
    };

    embeds[0]
        .addFields([
            {
                name: '**__!deploy__** (admin only!)',
                value: 'Deploy slashs commands to the guild',
            },
            {
                name: ':information_source:',
                value: 'All commands use slashs commands. type / to see all available commands',
            },
            {
                name: ':warning:',
                value: 'If the audio stops working, use the /leave command should fix the issue',
            },
        ])
        .setFooter(footerData)
        .setTimestamp();

    try {
        await msg.author.send(reply as unknown as MessagePayload);
    } catch (error) {
        // Unable to send DM
        await msg.reply(reply as unknown as MessagePayload);
    }
}

commands.set('deploy', deploy);
commands.set('help', help);
