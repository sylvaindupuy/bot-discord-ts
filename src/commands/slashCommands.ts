import ban from './slash/ban';
import clear from './slash/clear';
import join from './slash/join';
import leave from './slash/leave';
import pause from './slash/pause';
import play from './slash/play';
import playlist from './slash/playlist';
import remove from './slash/remove';
import resume from './slash/resume';
import skip from './slash/skip';
import unban from './slash/unban';
import version from './slash/version';

export const slashCommands = new Map();

slashCommands.set('play', play);
slashCommands.set('skip', skip);
slashCommands.set('playlist', playlist);
slashCommands.set('pause', pause);
slashCommands.set('resume', resume);
slashCommands.set('leave', leave);
slashCommands.set('version', version);
slashCommands.set('clear', clear);
slashCommands.set('remove', remove);
slashCommands.set('join', join);
slashCommands.set('ban', ban);
slashCommands.set('unban', unban);
