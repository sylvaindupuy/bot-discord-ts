# Bot Discord TypeScript

TypeScript implementation of [node bot discord (deprecated)](https://gitlab.com/sylvaindupuy/botDiscord)

## Project

## Discord Token

This bot need a token.
To get this token you can go on [Discord's developer page](https://discordapp.com/developers).

## Run application

### Pass your discord Token and Youtube API key

The Token and the API key are passed to the bot using environnement variables

_linux_

```bash
export TOKEN='yourToken'
```

_Windows_

```bat
SET TOKEN=yourToken
```

## Run on docker

`docker run -e 'TOKEN'='yourToken' -v './data:/usr/src/app/data' sylvaindupuy/bot-discord-ts:latest`

> Volume allow to persist data

### Launch

```bash
npm install
npm start
```

## Changelog

The changelog is available [here](CHANGELOG.md)

> Sylvain Dupuy
