# Bot discord ts change-log

## v2.4.1

-   Restore use of sodium native

## v2.4.0

-   Update dependencies
-   Update discord.js to [v14.14.1](https://github.com/discordjs/discord.js/releases/tag/14.14.1)

## v2.3.0

-   Fix discordjs error
-   Update dependencies
-   Update discord.js to [v14.9.0](https://github.com/discordjs/discord.js/releases/tag/14.9.0)

## v2.2.3

-   Fix play-dl error
-   Fix intent message content
-   Update discord.js to [v14.6.0](https://github.com/discordjs/discord.js/releases/tag/14.6.0)

## v2.2.2

-   Fix autologout disconncting when playing

## v2.2.1

-   Fix discordjs Select min/max length

## v2.2.0

-   Update discord.js to [v14](https://github.com/discordjs/discord.js/releases/tag/14.2.0)
-   Implement auto logout after 5 minutes
-   Fixes (Handle play-dl search functions errors, join cmd)

## v2.1.1

-   Fix soundcloud

## v2.1.0

-   Change music library from youtube-dl to play-dl
    -   Fix memory link issues
    -   Allow Soundclound sounds (and playlist)
    -   Unlocks the limit of songs for google re-search
-   Add specific start

## v2.0.0

-   Update discord.js to [v13](https://github.com/discordjs/discord.js/releases/tag/13.8.0)
-   Redesign of the voice system (using new @discordjs/voice package)
-   Redesign using new discord slashs commands
-   Using enmap to store banned users (persistant storage)
-   Add prettier to format code
-   Commands : Remove volume (for performance) / add pause & resume
-   Redesign play command (keywords) : you can choose between 5 songs

## v1.4.1

-   Update ytdl-core to 4.9.0

## v1.4.0

-   Feature : add songs from a youtube playlist

## v1.3.6

-   Fix : update ytdl-core

## v1.3.5

-   Fix : update dockerFile version (LTS)

## v1.3.4

-   Fix : update ytdl-core

## v1.3.3

-   Fix : update docker node version
-   Fix : use new ytdl-core "videoDetails"

## v1.3.2

-   Fix : update ytdl-core
-   Check permissions before suppress embed

## v1.3.1

-   Fix : Update node version (docker)
-   Update packages
-   Delete usage of pm2 (docker)

## v1.3.0

-   Upgrade to Discord.js [v12](https://github.com/discordjs/discord.js/releases/tag/12.0.0)
    > Use "@discordjs/opus" instead of node-opus
-   Refacto management of "global" variables
-   Suppress embeds on messages
-   Add new CI job to update production

## v1.2.2

-   Adding aliases for commands

## v1.2.1

-   Fix : remove allow_failure false

## v1.2.0

-   Feature : merging orders (!add and !play) in !play
-   Improve logs using colors
-   Optimize docker build + pipelines (remove deploy docker hub on develop)
-   Fix : fix licence in package json

## v1.1.2

-   Fix : 'audioonly' not working for some videos -> using 'highestaudio' (who prefer audioonly if possible)
-   Fix : Allow playing live stream
-   Fix : Error message on unknown video

## v1.1.1

-   Fix !version: change of version number retrieval

## v1.1.0

-   Redesign messages using discord RichEmbed

## v1.0.1

-   Add licence
-   Fix help command
-   Skip ci from tags

## v1.0.0

### First version

TypeScript implementation of [node bot discord (deprecated)](https://gitlab.com/sylvaindupuy/botDiscord)
