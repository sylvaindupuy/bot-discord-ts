FROM node:lts-alpine

ENV NODE_ENV='production'
ENV FORCE_COLOR=1

# Create app directory
WORKDIR /usr/src/app

# install needed package
RUN apk add --no-cache git python3 g++ make ffmpeg libtool autoconf automake

# Bind python3 to python
RUN ln -sf python3 /usr/bin/python

# copy 'package.json'
COPY package.json /usr/src/app

# install dependencies
RUN npm install --global bin-version-check-cli && npm install

# copy compiled project files
COPY dist/ /usr/src/app/dist/

# launch app on startup
CMD ["sh", "-c", "node dist/src/app.js"]
